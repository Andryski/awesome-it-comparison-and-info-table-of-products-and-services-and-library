# awesome IT comparison and info table of products and services and library

- [Site with learning Language and Coding Challenge](#Site-with-learning-Language-and-Coding-Challenge)
- [Online IDE](#Online-IDE)
- [mix](#mix)
 
## Online IDE

- https://github.com/styfle/awesome-online-ide

| Name | Language | LICENSE |
| -------- | -------- | -------- |
|[repl.it](https://repl.it/)|any|Free plan/Commercial|
|[ideone.com](https://ideone.com/)||Free plan/Commercial|
|[cloud9](https://aws.amazon.com/ru/cloud9/)|any|Free plan/Commercial|
|[ideone.com](https://ideone.com/)||Free plan/Commercial|
|[tutorialspoint](https://www.tutorialspoint.com/codingground.htm)||Free plan/Commercial|



## Site with learning Language and Coding Challenge

- https://github.com/mike-north/awesome-learn-to-code
- https://www.freecodecamp.org/news/the-10-most-popular-coding-challenge-websites-of-2016-fb8a5672d22f/

- https://www.codingame.com/
- https://goalkicker.com/
- https://www.codewars.com/
- https://codeforgeek.com/
- https://www.hackerrank.com/
- https://coderbyte.com/
- http://codeforces.com/
- https://checkio.org/
- https://www.codechef.com/
- https://leetcode.com/
- https://www.coderbyte.com/

## mix


| Name | Work on | info |
| -------- | -------- | -------- |
|[LXC](https://en.wikipedia.org/wiki/LXC) | Cgroup | work on is an os level virtualization method for running multiple isolated Linux systems (containers) on a control host using a single Linux kernel.|
|[Docker](https://www.docker.com/) | LXC | aplication level |
|[LXD](https://linuxcontainers.org/lxd/introduction/) | LXC | os level |
|[Cgroup]() | | |

- core os - contains minimal linux + docker
- Ubuntu Core - to do

- https://rollout.io/blog/container-os-comparison/
